package com.me.tamer.gameobjects.tiles.obstacles;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.me.tamer.core.Hud;
import com.me.tamer.gameobjects.Environment;
import com.me.tamer.gameobjects.creatures.Creature;
import com.me.tamer.gameobjects.creatures.Worm;
import com.me.tamer.gameobjects.renderers.EffectRenderer;
import com.me.tamer.gameobjects.renderers.RenderPool;
import com.me.tamer.gameobjects.renderers.Renderer;
import com.me.tamer.gameobjects.superclasses.DynamicObject;
import com.me.tamer.gameobjects.superclasses.StaticObject;
import com.me.tamer.utils.Helper;

public class Quicksand extends StaticObject implements Obstacle {
	private ArrayList<SandPart> parts;
	private ArrayList<Creature> submerged_creatures;
	private Vector2 bogHoleCenter;
	private Vector2 temp = new Vector2();
	
	public Quicksand() {
		parts = new ArrayList<SandPart>();
		submerged_creatures = new ArrayList<Creature>();
		bogHoleCenter = new Vector2();
	}

	public void setup(Environment environment) {
		environment.addObstacle(this);
	}

	public void addSandPart(SandPart p) {
		parts.add(p);
	}

	public void resolve(ArrayList<Creature> creatures) {
		int psize = parts.size();
		for (int k = 0; k < psize; k++) {
			for (int i = 0; i < creatures.size(); i++) {
				if(((DynamicObject) creatures.get(i)).getPosition().dst(parts.get(k).getPosition()) > 2)
					continue;
			if(submerged_creatures.contains(creatures.get(i)))
				continue;
				// Check if some wormhead is inside swamp
				if (creatures.get(i).getType() == Creature.TYPE_WORM && !((Worm) creatures.get(i)).isBound()) {
					Worm targetWorm = (Worm) creatures.get(i);
					if (targetWorm.getHead().isWithinRange(
							parts.get(k).getPosition(), 0.5f)) {
						// Set worm heading towards swamp, and prepare to dive.
						Vector2 heading = parts.get(k).getPosition().tmp()
								.sub(targetWorm.getHead().getPosition());
						targetWorm.setHeading(heading);
						targetWorm.submerge();
						// Remove worm from creatures, since its no longer in
						// game, yet it needs to be drawn and updated through
						// gameobjects.
						submerged_creatures.add(targetWorm);
					}
				}
			}
		}
		creatures.removeAll(submerged_creatures);
		submerged_creatures.clear();
	}

	@Override
	public void debugDraw(ShapeRenderer shapeRndr) {
		shapeRndr.setColor(1, 1, 1, 1);
		temp.set(Helper.worldToScreen(bogHoleCenter));
		shapeRndr.begin(ShapeType.Circle);
		shapeRndr.circle(temp.x, temp.y, 0.1f, 30);
		shapeRndr.end();
	}

	public boolean getDebug() {
		return false;
	}
}