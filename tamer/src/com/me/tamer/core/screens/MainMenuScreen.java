package com.me.tamer.core.screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.actions.AddAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.me.tamer.core.TamerGame;
import com.me.tamer.services.MusicManager.TamerMusic;

public class MainMenuScreen extends AbstractMenu{
	
	Image menuImage;

	public MainMenuScreen(final TamerGame game) {
		super(game);
		create();
	}
	
	@Override
	public void create(){
		super.create();
		
		menuImage = new Image(new Texture(
				Gdx.files.internal("data/graphics/menubg2.png")));
		
		menuImage.setFillParent(true);
		
		 // retrieve the default table actor
		Table table = super.getTable();
	    table.add( "" ).spaceBottom( Gdx.graphics.getHeight() / 5 );
	    table.row();
	
	    table.add( newGameButton ).size(Gdx.graphics.getWidth() /2, Gdx.graphics.getHeight() / 12).uniform().spaceBottom( 10 );
	    table.row();
	    table.add( levelsButton ).size(Gdx.graphics.getWidth() /2, Gdx.graphics.getHeight() / 12).uniform().spaceBottom( 10 );
	    table.row();
	    table.add( optionsButton ).size(Gdx.graphics.getWidth() /2, Gdx.graphics.getHeight() / 12).uniform().spaceBottom( 10 );
	    table.row();
	    
//	    menuImage = new Image(new Texture(
//				Gdx.files.internal("data/graphics/logofixed.png")));
	    
//	    menuImage.setFillParent(true);
	    
	    stage.addActor(menuImage);
	    stage.addActor(table);
	}
	
	@Override
	public void show(){
		super.show();
	}
}
